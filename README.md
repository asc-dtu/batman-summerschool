# Batman summerschool

Tutorials and exercises for the Batman summer school

## Setup

1. Login to surt.fysik.dtu.dk (probably through demon.fysik.dtu.dk)
2. Source the setup.sh script (currently by: `source /home/energy/stly/batman-summerschool/setup.sh`)
3. Start jupyter notebook with the command `notebook`
4. Create a tunnel through demon to surt
5. Open your browser and navigate to http://localhost:8888 where 8888 is the port used by jupyter
