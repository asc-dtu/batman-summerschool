{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "ein.tags": "worksheet-0",
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "# Introduction to ASE and GPAW"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "ein.tags": "worksheet-0",
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "ASE is a module designed for working with atoms. It uses the units of Ångstrom (Å) for length and electron volts (eV) for energy.\n",
    "\n",
    "In essence, ASE contains the `Atoms` object, which is a collection of `Atom` objects - thus, when we loop through the `Atoms` object, we get an `Atom` object. The `Atoms` object can then be associated with a so-called `calculator` object, which is just an object which knows how to calculate energies and forces, e.g. GPAW.\n",
    "\n",
    "<img src=\"ase-outline.png\">\n",
    "\n",
    "ASE and GPAW are quite complex modules, but there are good tutorials for doing many things, which can be found on their respective wiki pages.\n",
    "\n",
    "https://wiki.fysik.dtu.dk/ase/\n",
    "\n",
    "https://wiki.fysik.dtu.dk/gpaw/\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "ein.tags": "worksheet-0",
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "## Contents\n",
    "If you already are proficient in a topic you can skip it and move on to the next.\n",
    "\n",
    "- [ASE](#ase)\n",
    "- [GPAW](#gpaw)\n",
    "- [Submitting calculations](#submit)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "ein.tags": "worksheet-0",
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "## ASE\n",
    "<a id=\"ase\"></a>\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "autoscroll": false,
    "collapsed": false,
    "ein.hycell": false,
    "ein.tags": "worksheet-0",
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "# CO molecule with a bond length of 1.1 Å\n",
    "from ase import Atoms\n",
    "d = 1.1\n",
    "atoms = Atoms('CO', positions=[[0, 0, 0], [0, 0, d]])\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "ein.tags": "worksheet-0",
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "ASE contains tools to visualize the system. This opens a new window for viewing the atoms"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "autoscroll": false,
    "collapsed": false,
    "ein.hycell": false,
    "ein.tags": "worksheet-0",
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "from ase.visualize import view\n",
    "view(atoms)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "ein.tags": "worksheet-0",
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "We can loop through the `Atoms` object to get `Atom` objects"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "autoscroll": false,
    "collapsed": false,
    "ein.hycell": false,
    "ein.tags": "worksheet-0",
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "print(atoms)\n",
    "print(atoms.positions)\n",
    "for atom in atoms:\n",
    "    print(atom)\n",
    "    print(atom.index, atom.position)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "ein.tags": "worksheet-0",
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "As you can see, the first print statement is `Atoms`, which contains more than a single atom, while the `Atom` object only contains 1 atom. Both types of objects have variables that can be accessed directly (positions, magmoms, ...)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "ein.tags": "worksheet-0",
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "Let's try to setup a periodic structure."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "autoscroll": false,
    "collapsed": false,
    "ein.hycell": false,
    "ein.tags": "worksheet-0",
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "d = 2.9\n",
    "L = 10\n",
    "wire = Atoms('Au', positions=[[0, L / 2, L / 2]],\n",
    "             cell=[d, L, L],  # unit cell lengths\n",
    "             pbc=[1, 0, 0])  # periodic boundary conditions\n",
    "# let's try and repeat it and visualize primitive and repeated\n",
    "wire10 = wire * (10, 1, 1)\n",
    "view([wire, wire10])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "ein.tags": "worksheet-0",
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "Let's setup a surface using one of the utility functions in [`ase.build`](https://wiki.fysik.dtu.dk/ase/dev/ase/build/build.html#module-ase.build), add an [adsorbate](https://wiki.fysik.dtu.dk/ase/dev/ase/build/surface.html#adding-adsorbates), [fix](https://wiki.fysik.dtu.dk/ase/dev/ase/constraints.html#the-fixatoms-class) the \"bulk\" atoms and finally do a geometrical [relaxation](https://wiki.fysik.dtu.dk/ase/dev/ase/optimize.html#module-ase.optimize)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "autoscroll": false,
    "collapsed": false,
    "ein.hycell": false,
    "ein.tags": "worksheet-0",
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "# Create the slab\n",
    "from ase.build import fcc100\n",
    "\n",
    "slab = fcc100('Cu',\n",
    "              size=(3, 3, 3),\n",
    "              vacuum=7)\n",
    "view(slab)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "autoscroll": false,
    "collapsed": false,
    "ein.hycell": false,
    "ein.tags": "worksheet-0",
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "# Add an adsorbate\n",
    "from ase.build import add_adsorbate\n",
    "\n",
    "add_adsorbate(slab, adsorbate='Cu',\n",
    "              height=3.0,\n",
    "              position='ontop')\n",
    "\n",
    "view(slab)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "autoscroll": false,
    "collapsed": false,
    "ein.hycell": false,
    "ein.tags": "worksheet-0",
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "# Constrain the lower layers of the slab, they are the bulk\n",
    "from ase.constraints import FixAtoms\n",
    "\n",
    "con = FixAtoms(mask=[atom.tag > 1 for atom in slab])\n",
    "slab.set_constraint(con)\n",
    "\n",
    "view(slab)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "autoscroll": false,
    "collapsed": false,
    "ein.hycell": false,
    "ein.tags": "worksheet-0",
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "# Attach a calculator and relax the atomic positions\n",
    "from ase.calculators.emt import EMT\n",
    "from ase.optimize import BFGS\n",
    "\n",
    "# The calculator\n",
    "calc = EMT()\n",
    "slab.set_calculator(calc)\n",
    "\n",
    "# The optimizer\n",
    "traj_file = 'Cu-slab-relax.traj'\n",
    "opt = BFGS(slab, trajectory=traj_file)\n",
    "opt.run(fmax=0.05)  # unit of force is eV/Å\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "autoscroll": false,
    "collapsed": false,
    "ein.hycell": false,
    "ein.tags": "worksheet-0",
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "# view the steps of the relaxation\n",
    "from ase.io import read\n",
    "\n",
    "slab_relax = read(traj_file, index=':')\n",
    "\n",
    "view(slab_relax)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "autoscroll": false,
    "collapsed": false,
    "ein.hycell": false,
    "ein.tags": "worksheet-0",
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "# add small pertubation away from the symmetric position of the adsorbate\n",
    "slab[-1].position += [.1, .1, 0]\n",
    "opt.run(fmax=0.05)\n",
    "slab_relax = read(traj_file, index=':')\n",
    "\n",
    "view(slab_relax)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "autoscroll": false,
    "collapsed": false,
    "ein.hycell": false,
    "ein.tags": "worksheet-0",
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "# get the energy out directly\n",
    "print(slab.get_potential_energy())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "ein.tags": "worksheet-0",
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "## GPAW\n",
    "<a id=\"gpaw\"></a>\n",
    "\n",
    "[GPAW](https://wiki.fysik.dtu.dk/gpaw/index.html) is a density functional theory code written primarily in Python. It is based on the projector augmented wave (PAW) method. 3 different methods to describe the wave functions; plane wave (`mode=pw`), linear combination of atomic orbitals (`mode=lcao`) and on a real-space uniform grids with the finite-difference approximation (`mode=fd`)."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "autoscroll": false,
    "collapsed": false,
    "ein.hycell": false,
    "ein.tags": "worksheet-0",
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "# How to import the GPAW calculator\n",
    "from gpaw import GPAW\n",
    "\n",
    "calc = GPAW(h=0.24,\n",
    "            mode='lcao',\n",
    "            basis='sz(dzp)',\n",
    "            xc='PBE')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "ein.tags": "worksheet-0",
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "Let's calculate the DFT adsorption energy of CO on Cu(100) ontop site.\n",
    "\n",
    "Adsorption energy is defined: $E_{ads} = E_{Cu+CO} - (E_{Cu} + E_{CO})$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "autoscroll": false,
    "collapsed": false,
    "ein.hycell": false,
    "ein.tags": "worksheet-0",
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "#%%writefile CO.py\n",
    "# First check your structure is correct\n",
    "from ase.build import molecule\n",
    "from ase.optimize import BFGS\n",
    "mol = molecule('CO')\n",
    "mol.center(vacuum=10)\n",
    "\n",
    "view(mol)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "autoscroll": false,
    "collapsed": false,
    "ein.hycell": false,
    "ein.tags": "worksheet-0",
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "# Then attach the calculator\n",
    "from gpaw import GPAW\n",
    "calc = GPAW(h=0.24,\n",
    "            mode='lcao',\n",
    "            basis='sz(dzp)',\n",
    "            xc='PBE')\n",
    "\n",
    "mol.set_calculator(calc)\n",
    "opt = BFGS(mol, trajectory='CO.traj')\n",
    "opt.run(fmax=0.05)  # unit of force is eV/Å\n",
    "\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "ein.tags": "worksheet-0",
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "If you feel that the calculation of the two atoms above took plenty of time and you are unsure about the following calculations having 12 and 14 atoms, you can first read the section about [submitting calculations](#submit) to the compute cluster.\n",
    "\n",
    "The following cell is made ready for viewing the structure to check that it is correctly set up. When you are satisfied uncomment the lines regarding the real GPAW calculation. Also remember to comment out the view command."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "autoscroll": false,
    "collapsed": false,
    "ein.hycell": false,
    "ein.tags": "worksheet-0",
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "#%%writefile Cu-slab.py\n",
    "from ase.build import fcc100\n",
    "from ase.build import add_adsorbate\n",
    "from ase.build import molecule\n",
    "from ase.constraints import FixAtoms\n",
    "from gpaw import GPAW\n",
    "from ase.optimize import BFGS\n",
    "\n",
    "mol = molecule('CO')\n",
    "slab = fcc100('Cu',\n",
    "              size=(2, 2, 3),\n",
    "              vacuum=7)\n",
    "con = FixAtoms(mask=[atom.tag > 1 for atom in slab])\n",
    "slab.set_constraint(con)\n",
    "\n",
    "# calc = GPAW(h=0.24,\n",
    "#             mode='lcao',\n",
    "#             basis='sz(dzp)',\n",
    "#             xc='PBE',\n",
    "#             kpts=(6, 6, 1))\n",
    "\n",
    "# calc.set(txt='slab.txt')\n",
    "# slab.set_calculator(calc)\n",
    "# opt = BFGS(slab, trajectory='Cu-clean.traj')\n",
    "# opt.run(fmax=0.05)\n",
    "add_adsorbate(slab, adsorbate=mol,\n",
    "              height=1.8,\n",
    "              position='ontop')\n",
    "\n",
    "view(slab)\n",
    "\n",
    "# calc.set(txt='slab-and-adsorbate.txt')\n",
    "# opt = BFGS(slab, trajectory='Cu-adsorbate.traj')\n",
    "# opt.run(fmax=0.05)\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "autoscroll": false,
    "collapsed": false,
    "ein.hycell": false,
    "ein.tags": "worksheet-0",
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "# Submitting Cu-slab to the cluster.\n",
    "!submit.py -t 1 Cu-slab.py"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "autoscroll": false,
    "collapsed": false,
    "ein.hycell": false,
    "ein.tags": "worksheet-0",
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "e_Cu_ads = read('Cu-adsorbate.traj').get_potential_energy()\n",
    "e_Cu = read('Cu-clean.traj').get_potential_energy()\n",
    "e_mol = read('CO.traj').get_potential_energy()\n",
    "print(\"DFT adsorption energy: E_ads = {:f.3}\".format(e_Cu_ads - (e_Cu + e_mol)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "ein.tags": "worksheet-0",
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "## Submitting calculations\n",
    "<a id=\"submit\"></a>\n",
    "\n",
    "Often, DFT calculations take a while to run. A normal execution of a python program will only run on 1 core, however, that is very rarely ever enough to do any real simulations, so we turn to parallel execution of the programs.\n",
    "\n",
    "We will submit our parallel calculations to a queueing system asking for a number of processors. Our computation will start when there is ressources available.\n",
    "\n",
    "We have set up a program called `submit`, which will take care of submitting to the queue. The syntax in the terminal is\n",
    "\n",
    "```bash\n",
    "submit -t T myscript.py\n",
    "```\n",
    "which will submit `myscript.py` to the queue for the duration of `T` hours on one compute node with 16 processors. So for example, it could look something like\n",
    "```bash\n",
    "submit -t 1 myscript.py\n",
    "```\n",
    "which would submit `myscript.py` to the queue for 1 hour on 16 processors.\n",
    "\n",
    "Writing the script `myscript.py` can be done by using the magic command `%%writefile myscript.py` as the first line of a cell.\n",
    "\n",
    "We can then look at our queue with the command \n",
    "\n",
    "```bash\n",
    "qstat -u $USER\n",
    "```\n",
    "which gives us information about the jobs we currently have in the queue, whether they are waiting to start, running or completed. You can delete a job from the job with the command\n",
    "\n",
    "```bash\n",
    "qdel JOBID\n",
    "```\n",
    "where `JOBID` is the ID number of the job, which we can get with the `qstat` command above.\n",
    "\n",
    "*Note* that terminal commands can be run in a Jupyter notebook cell by prefixing the command by an explamation mark (!). The submit command from above would be:\n",
    "\n",
    "```\n",
    "!submit -t 1 myscript.py\n",
    "```"
   ]
  }
 ],
 "metadata": {
  "name": "ase-gpaw.ipynb"
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
