{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Fitting Potential Energy Surfaces with Machine Learning"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In this tutorial we will demonstrate how machine learning can be used to fit potential energy surfaces obtained from electronic structure calculations. This has the potential to greatly speed up computational discovery of e.g. battery materials, since machine learning algorithms are typically orders of magnitude faster than conventional electronic structure methods. This notebook will explore the advantages and potential pitfalls of atomistic machine learning methods by fitting a neural network to a data set of a water molecule generated with Density Functional Theory (DFT). The notebook is concluded with a set of additional exercises, which encourages the reader to play around with the many adjustable parameters of the approach. <br> <br>\n",
    "The machine learning fits in this notebook are made with the Atomistic Machine Learning Package (AMP) [1], which is an open-source package for atomistic machine learning developed at Brown University. The basic idea behind AMP is shown in the workflow below. It works by taking a set of input structures calculated with an electronic structure method and then transforming these according to a symmetrization scheme that makes them suitable as inputs to a machine learning model. The transformed structures are subsequently fitted with a machine learning model such as a neural network or a kernel-based regression model to output energies and/or forces, which match those obtained from the electronic structure calculations. You can read more about AMP through the link: https://amp.readthedocs.io/en/latest/\n",
    "<br>\n",
    "<br>\n",
    "<br>\n",
    "<img src=\"AMP2.png\" width = \"300\" title=\"AMP Workflow\">\n",
    "<br>\n",
    "<br>\n",
    "<img src=\"AMP1.png\" width = \"900\" title=\"AMP Logo\">\n",
    "<br>\n",
    "<br>\n",
    "<br>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#Start by importing relevant modules\n",
    "\n",
    "#ase and python stuff\n",
    "from ase.io import read, write\n",
    "from ase.visualize import view\n",
    "%matplotlib inline\n",
    "import pylab as plt\n",
    "import numpy as np\n",
    "from ase.geometry import get_angles\n",
    "import random\n",
    "import os\n",
    "import shutil\n",
    "\n",
    "#AMP stuff\n",
    "from amp import Amp\n",
    "from amp.model.neuralnetwork import NeuralNetwork\n",
    "from amp.descriptor.gaussian import Gaussian, make_symmetry_functions\n",
    "from amp.model import LossFunction"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Data set of a water molecule\n",
    "We will consider a water molecule as sketched below. We have constructed a data set of 900 different water molecules by varying the H-O-H angle, $\\theta$, and the O-H bond distance, r, on an equidistant grid. Energies and forces have subsequently been calculated by employing the PBE functional [3] as implemented in the Vienna Ab Initio Simulation Package (VASP) [4]. By running the two cells below you can visualize the data set using the GUI available within the ASE package [5] and the potential energy surface obtained from the DFT calculations.\n",
    "<img src=\"water_molecule.png\" width = \"300\" title=\"Water Molecule\">\n",
    "<figcaption> Figure taken from [2].</figcaption>\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#Read in and visualize the data set with the ASE GUI\n",
    "images = read('water_molecule.traj', index = ':')\n",
    "view(images) "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#Visualize potential energy surface\n",
    "distances = []\n",
    "angles = []\n",
    "energies = []\n",
    "for atoms in images:\n",
    "    v1 = [atoms.get_positions()[2]- atoms.get_positions()[0]]\n",
    "    v2 = [atoms.get_positions()[1]- atoms.get_positions()[0]]\n",
    "    angle = get_angles(v1, v2)\n",
    "    distance = np.linalg.norm(v1)\n",
    "    distances.append(distance)\n",
    "    angles.append(angle)\n",
    "    energies.append(atoms.get_potential_energy())\n",
    "\n",
    "r = np.reshape(distances, (30,30))\n",
    "theta = np.reshape(angles, (30,30))\n",
    "E = np.reshape(energies, (30, 30))\n",
    "\n",
    "\n",
    "plt.contourf(r, theta, E, 30)\n",
    "plt.xlabel('r [Å]', fontsize = 20)\n",
    "plt.ylabel('θ [degrees]', fontsize = 20)\n",
    "cbar = plt.colorbar()\n",
    "cbar.set_label('Potential Energy [eV]', fontsize = 20, rotation=270, labelpad = 30)\n",
    "cbar.ax.tick_params(labelsize=15) \n",
    "fig = plt.gcf()\n",
    "plt.tick_params(labelsize = 15)\n",
    "size = fig.get_size_inches()\n",
    "fig.set_size_inches(size*1.8)\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Selecting the training and test set\n",
    "In order to perform machine learning on the water molecule data set, we must select a training set for fitting our machine learning model. The best fit to the data is of course obtained by selecting all 900 images, but that is not very interesting. A more interesting approach is to select only a small subset as training data and subsequently evaluate the predictions of the machine learning model on the full data set. This will give an idea of the interpolation and extrapolation capabilities of our machine learning approach. <br>\n",
    "By running the cell below a random subset of N_train images are selected from the parent data set and the chosen data points are visualized on the PES plot from before. We have here chosen a rather small value of N_train but it is encouraged to play around with this parameter in the end of the tutorial."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#Training images\n",
    "\n",
    "N_train = 10 #number of training images\n",
    "\n",
    "images_random = images.copy()\n",
    "random.shuffle(images_random)\n",
    "train_images = images_random[0:N_train]\n",
    "\n",
    "\n",
    "#Visualize the previous potential energy surface with selected training points\n",
    "distances_train = []\n",
    "angles_train = []\n",
    "for atoms in train_images:\n",
    "    v1 = [atoms.get_positions()[2]- atoms.get_positions()[0]]\n",
    "    v2 = [atoms.get_positions()[1]- atoms.get_positions()[0]]\n",
    "    angle = get_angles(v1, v2)\n",
    "    distance = np.linalg.norm(v1)\n",
    "    distances_train.append(distance)\n",
    "    angles_train.append(angle)\n",
    "    \n",
    "    \n",
    "plt.contourf(r, theta, E, 30)\n",
    "plt.xlabel('r [Å]', fontsize = 20)\n",
    "plt.ylabel('θ [degrees]', fontsize = 20)\n",
    "cbar = plt.colorbar()\n",
    "cbar.set_label('Potential Energy [eV]', fontsize = 20, rotation=270, labelpad = 30)\n",
    "cbar.ax.tick_params(labelsize=15) \n",
    "fig = plt.gcf()\n",
    "plt.tick_params(labelsize = 15)\n",
    "size = fig.get_size_inches()\n",
    "fig.set_size_inches(size*1.8)\n",
    "plt.scatter(distances_train, angles_train, color ='r', marker = 'x', s = 100)\n",
    "plt.xlim((min(distances), max(distances)))\n",
    "plt.ylim((min(angles), max(angles)))\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Fingerprinting the training data\n",
    "In order to make the training data suitable as input to a regression model, we will need to fingerprint it. In short, this means taking the raw cartesian coordinates of the structures and transforming them in a way that leaves the coordinates invariant to translation and rotation. There exist several possible symmetrization schemes in litterature. We will here employ a Gaussian fingerprinting scheme as suggested by Behler et. al [6]. The theory behind these fingerprints are described thoroughly in the AMP documentation: https://amp.readthedocs.io/en/latest/theory.html <br> For our purpose, it is enough to know that there are two types of Gaussian fingerprints, G2 and G4, which take 2-body and 3-body interactions for each atom into account, respectively. These are displayed in the figure below. The Gaussian descriptors are parametrized by the parameters $\\eta, \\zeta, \\gamma$, which serve as hyperparameters for fingerprinting the chemical environment of each atom. Furthermore one has to specify a cutoff radius, which determines the extent of the local chemical environment of each atom. The larger cutoff and the more values of $\\eta, \\zeta, \\gamma$ that are included, the more detailed the description of the chemical environments typically becomes. This comes at a cost however, since this also makes the machine learning model more computationally demanding. It is encouraged to play around with the fingerprinting at the end of this tutorial.\n",
    "\n",
    "<br>\n",
    "<br>\n",
    "<img src=\"AMP3.png\" width = \"1100\" title=\"AMP Workflow\">\n",
    "<br>\n",
    "<br>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#Fingerprints\n",
    "elements = ['H', 'O'] #Fingerprints will be generated for the elements H and O\n",
    "\n",
    "\n",
    "G = make_symmetry_functions(elements=elements, type='G2',\n",
    "                             etas=[0.005, 4.0, 20.0, 80.0]) #This function generates G2-type fingerprints for the H and O atoms using the values of eta chosen\n",
    "\n",
    "\n",
    "G += make_symmetry_functions(elements=elements, type='G4',\n",
    "                             etas=[0.005],\n",
    "                             zetas=[1., 4.],\n",
    "                             gammas=[+1., -1.]) #This function generates G4-type fingerprints for the H and O atoms using the values of eta, lambda and gamma chosen\n",
    "\n",
    "cutoff = 4.0 #Cutoff radius for the Gaussian fingerprints"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Fitting the data with a neural network\n",
    "We are now ready to fit our data with a neural network. By running the cell below a neural network with two hidden layers and 5 nodes in each is fitted to the data. We have in this case chosen to fit only the energies in the data set but at the end of this tutorial it is encouraged to try including forces in the fit also. This is done by changing the value of force_rmse to some specified threshold. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#Neural network\n",
    "hiddenlayers = (5,5)\n",
    "\n",
    "#Convergence parameters for the fit. \n",
    "energy_rmse = 0.005 #root-mean-square errors of the energies\n",
    "force_rmse = None #root-mean-square error of the forces. Change this enable force training.\n",
    "energy_maxresid = None #maximum residual of energies\n",
    "force_maxresid = None  #maximum residual of forces\n",
    "\n",
    "convergence = {'energy_rmse': energy_rmse,\n",
    "               'force_rmse': force_rmse,\n",
    "               'energy_maxresid': energy_maxresid,\n",
    "               'force_maxresid': force_maxresid}\n",
    "\n",
    "\n",
    "\n",
    "calc = Amp(descriptor = Gaussian(Gs = G, cutoff = cutoff),\n",
    "           cores = 1,\n",
    "           model = NeuralNetwork(hiddenlayers = hiddenlayers))\n",
    "\n",
    "calc.model.lossfunction = LossFunction(convergence = convergence)\n",
    "\n",
    "print('Training initated.. this will take a few seconds...')\n",
    "calc.train(images = train_images)\n",
    "print('Training completed.')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Inspecting the training sesssion\n",
    "During the training session the results were written the .txt file amp-log.txt. This file contains information about our model and shows the loss function optimization that happens when the neural network is fitted to the data. By running the cell below the contents of the file will be printed and may be inspected."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#Inspecting the training session\n",
    "\n",
    "f = open('amp-log.txt', 'r')\n",
    "filecontents = f.read()\n",
    "print(filecontents)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Visualizing the Results\n",
    "Having fitted our machine learning model we are now ready to evaluate its performance on the full data set. This is done by loading the trained machine learning model saved as amp.amp. This file works as an ASE calculator, which can be used to calculate forces and energies for a structure through the commands atoms.get_potential_energy() and atoms.get_forces(). In the cells below we plot the potential energy surface of our original DFT data set along with the one predicted by AMP. The predicted minimum energy structure for each model is indicated with an asterisk. The final plot shows the absolute difference between the two along with a scatter plot of our training points. Where does our machine learning model perform well and where does it fail? Try running the notebook a few times and see if the conclusions change. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#Load AMP calculator and predict energies for entire data set\n",
    "calc = Amp.load('amp.amp', cores = 1)\n",
    "\n",
    "images_amp = [atoms.copy() for atoms in images]\n",
    "energies_amp = []\n",
    "for atoms in images_amp:\n",
    "    atoms.set_calculator(calc)\n",
    "    energies_amp.append(atoms.get_potential_energy())\n",
    "\n",
    "E_amp = np.reshape(energies_amp, (30, 30))\n",
    "\n",
    "\n",
    "#Get minimum DFT energy\n",
    "E_min = np.min(E)\n",
    "r_min = r[np.unravel_index(np.argmin(E, axis=None), E.shape)]\n",
    "theta_min = theta[np.unravel_index(np.argmin(E, axis=None), E.shape)]\n",
    "\n",
    "#Get minimum AMP energy\n",
    "E_amp_min = np.min(E_amp)\n",
    "r_amp_min = r[np.unravel_index(np.argmin(E_amp, axis=None), E_amp.shape)]\n",
    "theta_amp_min = theta[np.unravel_index(np.argmin(E_amp, axis=None), E_amp.shape)]\n",
    "\n",
    "\n",
    "#Plot DFT PES\n",
    "plt.contourf(r, theta, E, 30)\n",
    "plt.xlabel('r [Å]', fontsize = 20)\n",
    "plt.ylabel('θ [degrees]', fontsize = 20)\n",
    "cbar = plt.colorbar()\n",
    "cbar.set_label('Potential Energy [eV]', rotation=270, labelpad = 30, fontsize = 20)\n",
    "cbar.ax.tick_params(labelsize=15) \n",
    "fig = plt.gcf()\n",
    "plt.tick_params(labelsize = 15)\n",
    "size = fig.get_size_inches()\n",
    "fig.set_size_inches(size*1.8)\n",
    "plt.title('PES obtained with DFT', fontsize = 25)\n",
    "plt.scatter(r_min, theta_min, marker='*', color='white', s = 100)\n",
    "plt.text(r_min + 0.05, theta_min + 0.05, '{:.{}f}eV'.format(E_min, 4), fontsize = 20, color = 'white')\n",
    "plt.show()\n",
    "\n",
    "#Plot AMP PES\n",
    "plt.contourf(r, theta, E_amp, 30)\n",
    "plt.xlabel('r [Å]', fontsize = 20)\n",
    "plt.ylabel('θ [degrees]', fontsize = 20)\n",
    "cbar = plt.colorbar()\n",
    "cbar.set_label('Potential Energy [eV]', rotation=270, labelpad = 30, fontsize = 20)\n",
    "cbar.ax.tick_params(labelsize=15) \n",
    "fig = plt.gcf()\n",
    "plt.tick_params(labelsize = 15)\n",
    "size = fig.get_size_inches()\n",
    "fig.set_size_inches(size*1.8)\n",
    "plt.title('PES obtained with AMP', fontsize = 25)\n",
    "plt.scatter(r_amp_min, theta_amp_min, marker='*', color='white', s = 100)\n",
    "plt.text(r_amp_min + 0.05, theta_amp_min + 0.05, '{:.{}f}eV'.format(E_amp_min, 4), fontsize = 20, color = 'white')\n",
    "plt.show()\n",
    "\n",
    "#Plot absolute difference\n",
    "plt.contourf(r, theta, np.absolute(E_amp-E), 30)\n",
    "plt.xlabel('r [Å]', fontsize = 20)\n",
    "plt.ylabel('θ [degrees]', fontsize = 20)\n",
    "cbar = plt.colorbar()\n",
    "cbar.set_label('Energy difference [eV]', rotation=270, labelpad = 30, fontsize = 20)\n",
    "cbar.ax.tick_params(labelsize=15) \n",
    "fig = plt.gcf()\n",
    "plt.tick_params(labelsize = 15)\n",
    "size = fig.get_size_inches()\n",
    "fig.set_size_inches(size*1.8)\n",
    "plt.title('Absolute Difference', fontsize = 25)\n",
    "plt.scatter(distances_train, angles_train, color ='r', marker ='x', s=100)\n",
    "plt.xlim((min(distances), max(distances)))\n",
    "plt.ylim((min(angles), max(angles)))\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Cleaning up \n",
    "During training and model evaluation, AMP will generate a lot of files, which we need to remove before we redo any calculations. Running the cell below will do this. Whenever you get an error, try to run this command and see if the error persists. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#Clean up files generated by AMP\n",
    "os.system('rm -r amp*');"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Additional Exercises\n",
    "Now it is your turn to play around! There are several parameters in our approach that can be tweaked to give different results and insights. Below is a list of suggested ones to play around with.\n",
    "<body>\n",
    "    <ul>\n",
    "      <li> Training set size: Try to use different training set sizes and see how it affects the outcome. How many training points do we need to achieve a perfect fit of the potential energy surface? </li> <br> \n",
    "      <li>Force training: In the fits above we fitted only the energies of the structures. However, more information can be included, if we choose to also fit the forces on the atoms. This can be done by changing the value of force_rmse to a desired accuracy, but bear in mind that this will make training and model evaluation considerably slower! Does including forces improve our predictions? </li>  <br> \n",
    "      <li>Fingerprints: Try to play around with the cutoff and hyperparameters used to construct the fingerprints. Does e.g. increasing the cutoff influence the predictive performance of the model? </li> <br> \n",
    "      <li> The neural network size: Does a larger neural network improve the predictions? </li>\n",
    "    </ul>\n",
    "</body>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## References\n",
    "[1] A. Khorshidi, A.A. Peterson, Comput. Phys. Commun. 207 (2016) 310–324. <br>\n",
    "[2] Peterson AA, Christensen R, Khorshidi A: Phys Chem Chem Phys 2017, 19:10978–10985. <br>\n",
    "[3] J. P. Perdew, K. Burke, and M. Ernzerhof, Phys. Rev. Lett. 77, 3865 1996. <br>\n",
    "[4] G. Kresse and J. Hafner, Phys. Rev. B, vol. 47, pp. 558–561, Jan 1993. <br>\n",
    "[5] A. H. Larsen, J. J. Mortensen, J. Blomqvist, I. E. Castelli, R. Christensen, M. Duak, J. Friis, M. N. Groves, B. Hammer, C. Hargus, E. D. Hermes, P. C. Jennings, P. B. Jensen, J. K., J. R. Kitchin, E. L. Kolsbjerg, J. Kubal, K. Kaasbjerg, S. Lysgaard, J. B. Maronsson, T. Maxson, T. Olsen, L. Pastewka, A. Peterson, C. Rostgaard, J. Schiøtz, O. Schütt, M. Strange, K. S. Thygesen, T. Vegge, L. Vilhelmsen, M. Walter, Z. Zeng, and K. W. Jacobsen,” Journal of Physics: Condensed Matter, vol. 29, no. 27, pp. 273002, 2017. <br>\n",
    "[6] J. Behler, International Journal of Quantum Chemistry, vol. 115, no. 16, pp. 1032–1050, 2015."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
