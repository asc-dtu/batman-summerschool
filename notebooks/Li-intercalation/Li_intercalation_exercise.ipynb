{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Batteries in DFT\n",
    "\n",
    "In this first exercise,  we will take a look at graphite as an anode material, and how different functionals dramatically affect the results, even to a point where the simulation becomes incorrect.\n",
    "\n",
    "## Li intercalation energy in graphite\n",
    "\n",
    "Today we will calculate the energy cost/gain associated with intercalating a lithium atom into graphite using approaches at different levels of theory. After today you should be able to:\n",
    "\n",
    "- Setup structures and do relaxations using ASE and GPAW.\n",
    "- Discuss which level of theory is required to predict the Li intercalation energy and why?\n",
    "\n",
    "The Li intercalation reaction is:\n",
    "$$Li(s) + C_x^{graphite} \\rightarrow LiC_x$$\n",
    "and the intercalation energy will then be\n",
    "$$E_{Li@graphite} = E_{LiC_x} - (E_{Li(s)} + E_{C_x^{graphite}})$$\n",
    "Where $x$ is the number of Carbon atoms in your computational cell.\n",
    "\n",
    "We will calculate these energies using Density Functional Theory (DFT) with different exchange-correlation functionals. Graphite is characterised by graphene layers with strongly bound carbon atoms in hexagonal rings and weak long-range van der Waals interactions between the layers.\n",
    "\n",
    "You should do these calculations at three different functions, which each gets progressively more computationally demanding. \n",
    "* LDA\n",
    "* PBE\n",
    "* BEEF-vdW\n",
    "\n",
    "If you reach the end of the session on monday, without having done the PBE and BEEF-vdW calculations, please go to the end, and submit those calculations, so that you can analyze them tomorrow.\n",
    "\n",
    "You select which functional you are using by changing the `xc` keyword.\n",
    "\n",
    "**NOTE**: To execute a cell, press `Shift`+`Enter`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Graphite\n",
    "The first thing we need to do, is to optimize the structure of the graphite structure, which means determining the lattice constant and the interlayer distance.\n",
    "\n",
    "So we first set up a structure, which we can do in ASE."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# The graphite structure is set up using a tool in ASE\n",
    "from ase.lattice.hexagonal import Graphite\n",
    "\n",
    "# One has only to provide the lattice constants\n",
    "structure = Graphite('C', latticeconstant={'a': 1.5, 'c': 4.0})"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To verify that the structures is as expected, we can check it visually."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from ase.visualize import view\n",
    "\n",
    "view(structure)  # This will pop up the ase gui window"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you are satisfied with the look of your cell, we proceed to calculating the C-C distance.\n",
    "\n",
    "\n",
    "LDA is fast enough, that we don't need to submit these calculations to the queue, however, this will be necessary for PBE and BEEF-vdW, but we will see how to do this later (also to avoid overloading the interactive node).\n",
    "\n",
    "The procedure in this case, is to first optimize the C-C distance, and then the interlayer distance. We do this by calculaing the energy of the total system at different lattice parameters, and then finding the one which minimizes the energy.\n",
    "\n",
    "The code below takes around 1 minute to execute."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import time\n",
    "import numpy as np\n",
    "from ase.lattice.hexagonal import Graphite\n",
    "from gpaw import GPAW, PW\n",
    "\n",
    "xc = 'LDA'  # Choice of exchange-correlation functional\n",
    "\n",
    "ccdist = 1.40     # Guess for C-C distance\n",
    "layerdist = 3.33  # Guess for interlayer distance\n",
    "\n",
    "# Calculate C-C distances at +/- 20%\n",
    "dists = np.linspace(ccdist * .8, ccdist * 1.2, 10)\n",
    "energies = []\n",
    "\n",
    "for dist in dists:\n",
    "    t0 = time.time()\n",
    "    print('Running C-C distance: {:.3f} Å'.format(dist))\n",
    "    a = dist * np.sqrt(3)\n",
    "    c = 2 * layerdist\n",
    "    gra = Graphite('C', latticeconstant={'a': a, 'c': c})\n",
    "    \n",
    "    # Set up a calculator object, with some simulation accuracy parameters\n",
    "    # We will not worry too much about these here.\n",
    "    calc = GPAW(mode=PW(400),\n",
    "                kpts={'size': (5, 5, 3), 'gamma': True},\n",
    "                xc=xc,\n",
    "                txt=None)\n",
    "\n",
    "    gra.set_calculator(calc)  # Connect system and calculator\n",
    "    en = gra.get_potential_energy()  # This command will also execute the DFT calculation\n",
    "    t1 = time.time()\n",
    "    print('Energy: {:.6f} eV\\nExecution time: {:.2f} s\\n'.format(en, t1-t0))\n",
    "    energies.append(en)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Determine the equilibrium lattice constant. As a primitive fit, we we use a [np.polynomial.Polynomial](https://www.numpy.org/devdocs/reference/generated/numpy.polynomial.polynomial.Polynomial.html#numpy.polynomial.polynomial.Polynomial)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Fit a polynomial:\n",
    "poly = np.polynomial.Polynomial.fit(dists, energies, 3)\n",
    "# and plot it:\n",
    "%matplotlib notebook\n",
    "import matplotlib.pyplot as plt\n",
    "fig, ax = plt.subplots(1,\n",
    "                       1)\n",
    "ax.plot(dists, energies, '*r')\n",
    "x = np.linspace(1, 2, 100)\n",
    "ax.plot(x, poly(x))\n",
    "ax.set_xlabel('C-C distance [Ang]')\n",
    "ax.set_ylabel('Energy [eV]')\n",
    "plt.show()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We can then find the roots of the fitted curve to find the optimal C-C distance"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "poly1 = poly.deriv()\n",
    "poly1.roots()  # two extrema\n",
    "# Find the minimum:\n",
    "emin, ccdist = min((poly(d), d) for d in poly1.roots())\n",
    "print('Optimal C-C distance: {:.4f} Å'.format(ccdist))\n",
    "aopt = ccdist"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Next, we do a similar procedure for the interlayer distance. The following is a slightly condensed version of the above. Remember to update the `ccdist` variable to reflect your optimized C-C distance."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "from ase.lattice.hexagonal import Graphite\n",
    "from gpaw import GPAW, PW\n",
    "import time\n",
    "\n",
    "xc = 'LDA'\n",
    "\n",
    "ccdist = 1.40  # Insert your optimized C-C distance here\n",
    "layerdist = 3.33\n",
    "\n",
    "dists = np.linspace(layerdist * .8, layerdist * 1.2, 10)\n",
    "energies = []\n",
    "\n",
    "for ld in dists:\n",
    "    t0 = time.time()\n",
    "    print('Running interlayer distance: {:.3f} Å'.format(ld))\n",
    "    # Calculate the lattice parameters a and c from the C-C distance\n",
    "    # and interlayer distance, respectively\n",
    "    a = ccdist * np.sqrt(3)\n",
    "    c = 2 * ld\n",
    "\n",
    "    gra = Graphite('C', latticeconstant={'a': a, 'c': c})\n",
    "\n",
    "    # Set the calculator and attach it to the Atoms object with\n",
    "    # the graphite structure\n",
    "    calc = GPAW(mode=PW(400),\n",
    "                kpts={'size': (5, 5, 3), 'gamma': True},\n",
    "                xc=xc,\n",
    "                txt=None)\n",
    "    gra.set_calculator(calc)\n",
    "\n",
    "    en = gra.get_potential_energy()\n",
    "    t1 = time.time()\n",
    "    print('Energy: {:.6f} eV. Execution time: {:.2f} s'.format(en, t1-t0))\n",
    "    # Append results to the list\n",
    "    energies.append(en)\n",
    "    \n",
    "poly = np.polynomial.Polynomial.fit(dists, energies, 3)\n",
    "layerdist = next (d for d in poly.deriv().roots() if poly.deriv(2)(d) > 0)\n",
    "print()\n",
    "print('---------------')\n",
    "print('Optimized layer distance: {:.3f} Å'.format(layerdist))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, we need to get the energy of the optimized structure, which we will need later."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "from ase.lattice.hexagonal import Graphite\n",
    "from gpaw import GPAW, PW\n",
    "import time\n",
    "\n",
    "xc = 'LDA'\n",
    "\n",
    "# Insert your optimized C-C distance and layer distance here\n",
    "ccdist = 1.40 \n",
    "layerdist = 3.33\n",
    "\n",
    "a = ccdist * np.sqrt(3)\n",
    "c = 2 * layerdist\n",
    "\n",
    "gra = Graphite('C', latticeconstant={'a': a, 'c': c})\n",
    "\n",
    "calc = GPAW(mode=PW(400),\n",
    "            kpts={'size': (5, 5, 3), 'gamma': True},\n",
    "            xc=xc,\n",
    "            txt=None)\n",
    "\n",
    "gra.set_calculator(calc)\n",
    "\n",
    "en = gra.get_potential_energy()\n",
    "\n",
    "print('Energy of optimized graphite: {:.6f} eV'.format(en))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Li metal\n",
    "\n",
    "Now we need to calculate the energy of Li metal. We will use the same strategy as for graphite, i.e. first determine the lattice constant, then use the energy of that structure. \n",
    "\n",
    "To simplify the optimization of the lattice parameters, we can use the `StrainFilter` class, which will automate the process, by using the stress tensor."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from gpaw import GPAW, PW\n",
    "from ase.build import bulk\n",
    "from ase.constraints import StrainFilter\n",
    "from ase.optimize import BFGS\n",
    "\n",
    "# This script will optimize lattice constant of metallic lithium\n",
    "xc = 'LDA'  # xc-functional. We will be changing this later.\n",
    "Li_metal = bulk('Li', crystalstructure='bcc', a=3.3, cubic=True)\n",
    "\n",
    "calc = GPAW(mode=PW(400),\n",
    "            kpts={'size': (8, 8, 8), 'gamma':True},\n",
    "            nbands=-10,\n",
    "            txt='Li-metal-{}.log'.format(xc),\n",
    "            xc=xc)\n",
    "\n",
    "Li_metal.set_calculator(calc)\n",
    "\n",
    "sf = StrainFilter(Li_metal, mask=[1, 1, 1, 0, 0, 0])\n",
    "opt = BFGS(sf)\n",
    "opt.run(fmax=0.01)\n",
    "print('Energy: {:.6f}'.format(Li_metal.get_potential_energy()))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This will modify the atoms object `Li_metal`, so we can inspect the cell, for instance by using the GUI."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from ase.visualize import view\n",
    "\n",
    "view(Li_metal)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You should be able to determine the cell size from the GUI, but here is a another way of getting the information, using function calls instead."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "print(Li_metal.get_cell())\n",
    "print('Optimized lattice parameters:', np.linalg.norm(Li_metal.get_cell(), axis=1))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "How do these parameters compare to the experimental value of 3.51 Å?\n",
    "\n",
    "## Li intercalation in graphite\n",
    "\n",
    "Now we will calculate the intercalation of Li in graphite. For simplicity we will represent the graphite with only one layer. Also try and compare the C-C and interlayer distances to experimental values.\n",
    "\n",
    "First we construct an Atoms object, and visualize the object, to ensure that it looks reasonable."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from ase import Atom\n",
    "from ase.lattice.hexagonal import Graphene\n",
    "from ase.visualize import view\n",
    "\n",
    "ccdist = 1.40    # Initial guess\n",
    "layerdist = 3.7  # Initial guess\n",
    "\n",
    "a = ccdist * np.sqrt(3)\n",
    "c = layerdist\n",
    "\n",
    "# We will require a larger cell, to accomodate the Li\n",
    "Li_gra = Graphene('C', size=(2, 2, 1), latticeconstant={'a': a, 'c': c})\n",
    "Li_gra.append(Atom('Li', (a / 2, ccdist / 2, layerdist / 2)))\n",
    "\n",
    "view(Li_gra)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If you are reasonably satisfied with the above structure, we can continue with optimizing the structure."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from gpaw import GPAW, PW\n",
    "from ase import Atom\n",
    "from ase.optimize.bfgs import BFGS\n",
    "import numpy as np\n",
    "from ase.lattice.hexagonal import Graphene\n",
    "from ase.constraints import StrainFilter\n",
    "\n",
    "xc = 'LDA'\n",
    "ccdist = 1.40    # Initial guess\n",
    "layerdist = 3.7  # Initial guess\n",
    "\n",
    "a = ccdist * np.sqrt(3)\n",
    "c = layerdist\n",
    "\n",
    "# We will require a larger cell, to accomodate the Li\n",
    "Li_gra = Graphene('C', size=(2, 2, 1), latticeconstant={'a': a, 'c': c})\n",
    "Li_gra.append(Atom('Li', (a / 2, ccdist / 2, layerdist / 2)))\n",
    "\n",
    "calc = GPAW(mode=PW(400),\n",
    "            kpts={'size': (2, 2, 4), 'gamma': True},\n",
    "            xc=xc,\n",
    "            txt='Li-gra-{}.txt'.format(xc))\n",
    "\n",
    "Li_gra.set_calculator(calc)  # Connect system and calculator\n",
    "\n",
    "sf = StrainFilter(Li_gra, mask=[1, 1, 1, 0, 0, 0])\n",
    "opt = BFGS(sf)\n",
    "opt.run(fmax=0.01)\n",
    "\n",
    "print('Energy: {:.6f}'.format(Li_gra.get_potential_energy()))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "view(Li_gra) # Visualize the optimized structure, to find the C-C distance and interlayer spacing."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now calculate the intercalation energy of Li in graphite with the following formula:\n",
    "$$E_{Li@graphite} = E_{LiC_x} - (E_{Li(s)} + x * E_{C^{graphite}})$$\n",
    "where $x$ is the number of Carbon atoms in your Li intercalated graphene cell. Remember to adjust the energies so that the correct number of atoms is taken into account.\n",
    "\n",
    "Experimental C-C distance in graphite is 1.441 Å, and the interlayer distance is 3.706 Å.\n",
    "\n",
    "* How do the LDA C-C distance and interlayer distance compare to the experimental values?\n",
    "* What is the intercalation energy of Li in graphite with LDA? Hint: You need to use the energy from the previous (optimized) calculations.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Using higher levels of theory\n",
    "\n",
    "We now move away from LDA, and onto PBE and BEEF-vdW. As these calculations are considerably more expensive than the LDA ones, we will be submitting them to the cluster - this also means that we can improve the accuracy of the calculations a bit as well, as we will be using 16 cores for each calculation.\n",
    "\n",
    "### How to submit jobs?\n",
    "\n",
    "We will be submitting the jobs from the notebook. The procedure consits of two parts:\n",
    "\n",
    "1. Make a script, and writing that to file\n",
    "    * This is accomplished using the `%%writefile filename.py` \"magic\". Add this to the start of a cell, and it will write the entire cell as a new file called `filename.py`.\n",
    "2. Submit that script to the queue, and wait for it to finish\n",
    "    * This is done by using the `submit.py -t HH filename.py` command, which submits the script to for `HH` number of hours, e.g. `submit.py -t 1 myscript.py` would submit `myscript.py` to 16 processors for 1 hour.\n",
    "    * Commands are executed in the notebook using `!`, which then executed like a shell-command instead of python, e.g. `!submit.py -t 1 myscript.py` in a new cell.\n",
    "    \n",
    "## Graphite\n",
    "\n",
    "We will more-or-less repeat the procedure from before, but now we will change the `xc` keyword. You should do the following with the following exchange-correlation functionals.\n",
    "* `xc='PBE'`\n",
    "* `xc='BEEF-vdW'`\n",
    "\n",
    "The following script will optimize graphite, Li-metal as well as Li intercalated in graphite, all in 1 go. All you need to do, is update the `xc` keyword, so that we can analyze the results at a later stage - depending on how lucky you are with getting through the queue, it might not take long to get the results.\n",
    "\n",
    "**IMPORTANT** Remember to change the filename in the `%%writefile` statement, so that you don't overwrite the script on disk, e.g. to `%%writefile run_PBE.py`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile myscript.py\n",
    "import numpy as np\n",
    "from ase.lattice.hexagonal import Graphite, Graphene\n",
    "from ase.build import bulk\n",
    "from ase import Atom\n",
    "from gpaw import GPAW, PW\n",
    "from ase.optimize.bfgs import BFGS\n",
    "from ase.constraints import StrainFilter\n",
    "\n",
    "xc = 'LDA'  # Modify this, to match the XC functional you want to use\n",
    "\n",
    "# Calculate graphite\n",
    "#==========================================================\n",
    "ccdist = 1.40\n",
    "layerdist = 3.33\n",
    "\n",
    "a = ccdist * np.sqrt(3)\n",
    "c = 2 * layerdist\n",
    "gra = Graphite('C', latticeconstant={'a': a, 'c': c})\n",
    "calcname = 'graphite-{}'.format(xc)\n",
    "\n",
    "calc = GPAW(mode=PW(500),\n",
    "            kpts={'size': (10, 10, 6), 'gamma': True},\n",
    "            xc=xc,\n",
    "            txt=calcname + '.log')\n",
    "\n",
    "gra.set_calculator(calc)  # Connect system and calculator\n",
    "sf = StrainFilter(gra, mask=[1, 1, 1, 0, 0, 0])\n",
    "opt = BFGS(sf)\n",
    "opt.run(fmax=0.01)\n",
    "en = gra.get_potential_energy()  # This command will also execute the DFT calculation\n",
    "cell = gra.get_cell()\n",
    "\n",
    "# Write results to a file\n",
    "gra.write('graphite_relaxed_{}.traj'.format(xc))\n",
    "\n",
    "# Calculate Li metal\n",
    "#==========================================================\n",
    "\n",
    "Li_metal = bulk('Li', crystalstructure='bcc', a=3.3, cubic=True)\n",
    "\n",
    "calc = GPAW(mode=PW(500),\n",
    "            kpts={'size': (8, 8, 8), 'gamma':True},\n",
    "            nbands=-10,\n",
    "            txt='Li-metal-{}.log'.format(xc),\n",
    "            xc=xc)\n",
    "\n",
    "Li_metal.set_calculator(calc)\n",
    "\n",
    "sf = StrainFilter(Li_metal, mask=[1, 1, 1, 0, 0, 0])\n",
    "opt = BFGS(sf)\n",
    "opt.run(fmax=0.01)\n",
    "\n",
    "Li_metal.write('li_relaxed_{}.traj'.format(xc))\n",
    "\n",
    "# Calculate Li intercalated in graphite\n",
    "#==========================================================\n",
    "ccdist = 1.40    # Initial guess\n",
    "layerdist = 3.7  # Initial guess\n",
    "\n",
    "a = ccdist * np.sqrt(3)\n",
    "c = layerdist\n",
    "\n",
    "Li_gra = Graphene('C', size=(2, 2, 1), latticeconstant={'a': a, 'c': c})\n",
    "Li_gra.append(Atom('Li', (a / 2, ccdist / 2, layerdist / 2)))\n",
    "\n",
    "calc = GPAW(mode=PW(500),\n",
    "            kpts={'size': (5, 5, 6), 'gamma': True},\n",
    "            xc=xc,\n",
    "            txt='Li-gra-{}.txt'.format(xc))\n",
    "\n",
    "Li_gra.set_calculator(calc)  # Connect system and calculator\n",
    "\n",
    "sf = StrainFilter(Li_gra, mask=[1, 1, 1, 0, 0, 0])\n",
    "opt = BFGS(sf)\n",
    "opt.run(fmax=0.01)\n",
    "\n",
    "Li_gra.write('li_gra-{}.traj'.format(xc))\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!submit.py -t 1 myscript.py  # Remember to adjust script name to the new name you gave the above script. "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Use the below command to list your jobs in the queue, and see if they are in queue, running or completed."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!squeue -u $USER  # Show your job queue"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once the calculations are complete, we can load the structures back in for further analysis."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from ase.visualize import view\n",
    "from ase.io import read\n",
    "\n",
    "xc = 'PBE'\n",
    "gra = read('graphite_relaxed_{}.traj'.format(xc))\n",
    "Li_metal = read('li_relaxed_{}.traj'.format(xc))\n",
    "Li_gra = read('li_gra-{}.traj'.format(xc))\n",
    "\n",
    "print(gra.get_potential_energy())\n",
    "print(Li_metal.get_potential_energy())\n",
    "print(Li_gra.get_potential_energy())"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "view([gra, Li_metal, Li_gra])  # Visualize all 3 systems together"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now calculate the Li intercalation energy using PBE and BEEF-vdW, and compare them to the experimental and LDA values from before.\n",
    "* Which functional performs the best in getting the intercalation energy?\n",
    "* Which for the lattice parameters?\n",
    "* Do you see any big issues with any of the functionals?"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Transport barrier of Li in graphite\n",
    "\n",
    "In the following exercise, you will calculate the energy barrier for Li diffusion in the graphite anode. You will do this using the [Nudged Elastic Band (NEB) method](https://wiki.fysik.dtu.dk/ase/ase/neb.html#module-ase.neb)\n",
    "\n",
    "You can use your work from the previous exercise, but for simplicity you are advised to load in the initial atomic configuration from a file that we have prepared for you."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from ase.io import read\n",
    "\n",
    "initial = read('NEB_init.traj')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Visualize the structure"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from ase.visualize import view\n",
    "view(initial)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We will now make a final structure, where the Li atom has been moved to a neighbouring equivalent site. The [`get_positions`](https://wiki.fysik.dtu.dk/ase/ase/atoms.html?highlight=get_positions#ase.Atoms.get_positions), [`set_positions`](https://wiki.fysik.dtu.dk/ase/ase/atoms.html?highlight=get_positions#ase.Atoms.set_positions) and [`get_cell`](https://wiki.fysik.dtu.dk/ase/ase/atoms.html?highlight=get_positions#ase.Atoms.get_cell) functions are highly useful for such a task. We will displace the Li atom by $\\frac{1}{n} (\\vec{a}+\\vec{b})$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "final = initial.copy()\n",
    "cell = final.get_cell()\n",
    "pos = final.get_positions()\n",
    "pos[6] = pos[6] + cell[1] / 3 + cell[0] / 3\n",
    "final.set_positions(pos)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We of course remember to visualize the initial and final states"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "view([initial, final])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Make a band consisting of 7 images including the initial and final. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "images = [initial]\n",
    "images += [initial.copy() for i in range(5)]  # These will become the minimum energy path images.\n",
    "images += [final]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It this point `images` consist of 6 copies of `initial` and one entry of `final`. Use the `NEB` method to create an initial guess for the minimum energy path (MEP). In the cell below a simple interpolation between the `initial` and `final` image is used as initial guess."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from ase.neb import NEB\n",
    "neb = NEB(images)\n",
    "neb.interpolate()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Visualize the NEB pathway, to ensure that the initial guess looks reasonable."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "view(images)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It turns out, that while running the NEB calculation, the largest amount of resources will be spend translating the carbon layer without any noticeable buckling. You will thus [constrain](https://wiki.fysik.dtu.dk/ase/ase/constraints.html#constraints) the positions of the carbon atoms to save computational time. \n",
    "\n",
    "This very simple case is highly symmetric. To better illustrate how the NEB method works, the symmetry is broken using the [rattle](https://wiki.fysik.dtu.dk/ase/ase/atoms.html#ase.Atoms.rattle) function on the central image."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from ase.constraints import FixAtoms\n",
    "for image in images:\n",
    "    image.set_constraint(FixAtoms(mask=[atom.symbol == 'C' for atom in image]))\n",
    "images[3].rattle(stdev=0.05, seed=42)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Let's view the images again. Notice the X's over the carbon atoms, which indicates that they will be treated as contrained atoms, so they will not be allowed to move during the NEB optimization. Also, notice how the Li atom in the central image is moved slightly out of the perfectly straight line."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "view(images)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once we are satisfied with our NEB pathway, we store it as a new trajectory file, so that we can read it during the submitted job."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from ase.io import write\n",
    "write('neb_images.traj', images)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we are ready to construct our script, to run the job on the cluster. Remember to save the script to a new file, if you want to change the `xc` functional."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%writefile neb.py\n",
    "from ase.io import read\n",
    "from gpaw import GPAW, PW\n",
    "from ase.optimize import BFGS\n",
    "from ase.neb import NEB\n",
    "\n",
    "# Functional choice\n",
    "xc = 'LDA'\n",
    "\n",
    "# Load in the pathway we created\n",
    "images = read('neb_images.traj', index=':')\n",
    "\n",
    "neb = NEB(images)\n",
    "\n",
    "for image in images:\n",
    "    calc = GPAW(mode=PW(500),\n",
    "                kpts={'size': (5, 5, 6), 'gamma': True},\n",
    "                xc=xc,\n",
    "                symmetry={'point_group': False},\n",
    "                txt=None)\n",
    "    image.set_calculator(calc)\n",
    "    \n",
    "# Get energies of end point images, as these are not run during the NEB algorithm\n",
    "images[0].get_forces()\n",
    "images[-1].get_forces()\n",
    "\n",
    "# Run the NEB algorithm\n",
    "optimizer = BFGS(neb, trajectory='neb_{}.traj'.format(xc), logfile='neb_{}.log'.format(xc))\n",
    "optimizer.run(fmax=0.10)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Keep in mind, that NEB algorithms can take quite a long time, as we are simultaniously relaxing 5 images, although, in this case, we are contraining the carbon movement, so the calculations will finish in reasonable time. In practical applications, we easily spend days on these types of calculations.\n",
    "\n",
    "Due to this quite demanding type of calculations, researchers are constantly trying to find ways of reducing the time we spend doing NEB calculations, such as using the symmetry of the system, or applying machine learning techniques to learn the potential energy surface (PES) faster during the NEB run.\n",
    "\n",
    "Fortunately, due to our simplifications, in this case a regular NEB calculation will complete in a matter of minutes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!submit.py -t 1 neb.py"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "!squeue -u $USER  # Show the queue"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once the above is complete, you can visualize the results, and use the GUI tool to inspect the barrier. Run the below command, and click `Tools` &rarr; `NEB`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from ase.io import read\n",
    "from ase.visualize import view\n",
    "\n",
    "xc = 'LDA'\n",
    "\n",
    "# Read in the final results of the NEB optimization, and visualize\n",
    "neb_results = read('neb_{}.traj'.format(xc), index='-7:')\n",
    "view(neb_results)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "* What is the potential energy barrier for Li diffusion in graphite?\n",
    "* What does the final Li diffusion pathway look like?\n",
    "\n",
    "Finally, let's just try to visualize the relaxation procedure, by loading in the entire trajectory, instead of the final run, to see how the pathway changes:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "xc = 'LDA'\n",
    "view(read('neb_{}.traj'.format(xc), index=':'))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Once you are done, try changing the `xc` keyword, and see how the diffusion barrier changes. Can we just use any functional, and expect identical results?"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
