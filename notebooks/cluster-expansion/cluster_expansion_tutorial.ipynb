{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "ein.tags": "worksheet-0",
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "# Cluster Expansion on Au-Cu Alloy Using CLEASE"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "ein.tags": "worksheet-0",
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "## Introduction\n",
    "\n",
    "In this tutorial, we show you show how to construct a Cluster Expansion (CE) model using CLEASE. We will look into Au-Cu alloys as it is a good example system to illustrate how one can use CE. It is known that Au-Cu alloy has 3 stable ordered phases at low temperatures: Au$_3$Cu, AuCu and AuCu$_3$. Thermodynamic quantities of the Au-Cu alloy calculated using CE-based Monte Carlo simulations are shown below. The three stable ordered phases can be seen in formation energy and free energy of formation plots.\n",
    "\n",
    "<img src=\"thermo.png\" style=\"width: 400px;\"/>\n",
    "\n",
    "It is noted that one can construct a CE model of Au-Cu alloy using around 30 to 40 DFT calculations, followed by rigorous Monte Carlo sampling (thousand atoms or more in a cell, millions of MC sampling per composition and temperature) to construct the plots above. As such an approach is not feasible in this tutorial due to the time constraint, we use the ``EMT`` calculator for calculating energies and search for the ground-state structures of the alloy in order to construct the formation energy plot of Au-Cu alloy at 0 K.\n",
    "\n",
    "Note1: Since we are using ``EMT`` calculator, we should expect some difference from the plots shown above. \n",
    "\n",
    "Note2: For best experience, it is recommended to install ``spglib``, ``numba`` and ``scikit-learn`` (scikit-learn dependency to be removed).\n",
    "\n",
    "\n",
    "## Specify the concentration ranges of species\n",
    "\n",
    "The first step in setting up CE in ASE is to specify the types of elements occupying each basis and their concentration ranges using the `Concentration` class. For AuCu alloys, we consider the entire composition range of Au$_x$Cu$_{1-x}$ where $0 \\leq x \\leq 1$. The `Concentration` object can be created as below because there is no restriction imposed on the concentration range."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "autoscroll": false,
    "ein.hycell": false,
    "ein.tags": "worksheet-0",
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "from ase.clease import Concentration\n",
    "\n",
    "conc = Concentration(basis_elements=[['Au', 'Cu']])\n",
    "print(\"Done\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "ein.tags": "worksheet-0",
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "A nested list is passed to the ``basis_elements`` argument because the constituting elements are specified per basis, and FCC (crystal structure of Au$_x$Cu$_{1-x}$ for all $0 \\leq x \\leq 1$) has only one basis. The initialization automatically creates a linear algebra representation of the default concentration range constraints. \n",
    "\n",
    "The equality condition of $A_\\mathrm{eq} = \\begin{bmatrix} \\begin{bmatrix} 1 & 1 \\end{bmatrix} \\end{bmatrix}$ and $b_\\mathrm{eq} = \\begin{bmatrix} 1 \\end{bmatrix}$ as well as the lower bound conditions of $A_\\mathrm{lb} = \\begin{bmatrix} \\begin{bmatrix}1 & 0\\end{bmatrix}, \\begin{bmatrix}0 & 1\\end{bmatrix} \\end{bmatrix}$ and $b_\\mathrm{lb} = \\begin{bmatrix}0 & 0\\end{bmatrix}$ are created automatically. \n",
    "\n",
    "The conditions represents the linear equations $A_\\mathrm{eq} c_\\mathrm{species} = b_\\mathrm{eq}$ and $A_\\mathrm{lb} c_\\mathrm{species} \\geq b_\\mathrm{lb}$, where the concentration list, $c_\\mathrm{species}$, is defined as $c_\\mathrm{species} = \\begin{bmatrix} c_\\mathrm{Au} & c_\\mathrm{Cu} \\end{bmatrix}$.\n",
    "\n",
    "The equality condition is then expressed as $c_\\mathrm{Au} + c_\\mathrm{Cu} = 1$, which specifies that elements Au and Cu constitute the entire basis (only one basis in this case). The lower bound conditions are expressed as $c_\\mathrm{Au} \\geq 0$ and $c_\\mathrm{Cu} \\geq 0$, which speicify that the concentrations of Au and Cu must be greater than or equal to zero.\n",
    "\n",
    "You can check the values of $A_\\mathrm{eq}$, $b_\\mathrm{eq}$, $A_\\mathrm{lb}$ and $b_\\mathrm{lb}$ as below.   "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "autoscroll": false,
    "ein.hycell": false,
    "ein.tags": "worksheet-0",
    "scrolled": true,
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "print(\"A_eq = {}\\n\"\n",
    "      \"b_eq = {}\\n\"\n",
    "      \"A_lb = {}\\n\"\n",
    "      \"b_lb = {}\".format(conc.A_eq, conc.b_eq, conc.A_lb, conc.b_lb))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Little bit more advanced example\n",
    "\n",
    "\n",
    "The AuCu system presented in this tutorial does not impose any concentration constraints. However, we demonstrate how one can impose extra constraints by using an example case where the concentration of interest is Au$_x$Cu$_{1-x}$ where $0 \\leq x \\leq 0.5$. The extra concentration constraint can be specified in one of three ways.\n",
    "\n",
    "The first method is to specify the extra constraint using ``A_eq``, ``b_eq``, ``A_lb`` and ``b_lb``. For this particular case, the extra constraint is specified using ``A_lb`` and ``b_lb`` arguments as:\n",
    "\n",
    "```python\n",
    "from ase.clease import Concentration\n",
    "\n",
    "conc = Concentration(basis_elements=[['Au', 'Cu']],\n",
    "                     A_lb=[[2, 0]], b_lb=[1])\n",
    "```\n",
    "\n",
    "The second method is to specify the concentration range using formula unit strings. The `Concentration` class contains `set_conc_formula_unit()` method which accepts formula strings and variable range, which can be invoked as:\n",
    "\n",
    "```python\n",
    "from ase.clease import Concentration\n",
    "\n",
    "conc = Concentration(basis_elements=[['Au', 'Cu']])\n",
    "conc.set_conc_formula_unit(formulas=[\"Au<x>Cu<1-x>\"],\n",
    "                           variable_range={\"x\": (0, 0.5)})\n",
    "```\n",
    "\n",
    "The last method is to specify the concentration range each constituting species using `set_conc_ranges()` method. The lower and upper bound of species are specified in a nested list inthe same order as the ``basis_elements`` as:\n",
    "\n",
    "```python\n",
    "from ase.clease import Concentration\n",
    "\n",
    "conc = Concentration(basis_elements=[['Au', 'Cu']])\n",
    "conc.set_conc_ranges(ranges=[[(0, 0.5), (0.5, 1)]])\n",
    "```\n",
    "\n",
    "The above three methods yields the same results where $x$ is constrained to $0 \\leq x \\leq 0.5$."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "ein.tags": "worksheet-0",
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "## Specify CE settings\n",
    "\n",
    "The next step is to specify the settings in which the CE model is constructed.\n",
    "One of `CEBulk` or `CECrystal` classes is used to specify the\n",
    "settings. `CEBulk` class is used when the crystal structure is one of\n",
    "\"sc\", \"fcc\", \"bcc\", \"hcp\", \"diamond\", \"zincblende\", \"rocksalt\",\n",
    "\"cesiumchloride\", \"fluorite\" or \"wurtzite\".\n",
    "\n",
    "Here is how to specify the settings for performing CE on\n",
    "Au$_x$Cu$_{1-x}$ for all $0 \\leq x \\leq 1$ on FCC lattice with a\n",
    "lattice constant of 3.8 Å:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "autoscroll": false,
    "ein.hycell": false,
    "ein.tags": "worksheet-0",
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "from ase.clease import CEBulk\n",
    "\n",
    "setting = CEBulk(crystalstructure='fcc',\n",
    "                 a=3.8,\n",
    "                 supercell_factor=64,\n",
    "                 concentration=conc,\n",
    "                 db_name=\"aucu.db\",\n",
    "                 max_cluster_size=4,\n",
    "                 max_cluster_dia=[6.0, 4.5, 4.5])\n",
    "print(\"Done\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "ein.tags": "worksheet-0",
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "`CEBulk` takes ``crystalstructure`` and ``a`` arguments which correspond to the type of crystal structure and lattice constant in Å, respectively. ``supercell_factor`` argument is used to set a threshold on the maximum size of the supercell for generating training structures. In case where one prefers to perform CE on a single, fixed size supercell, ``size`` parameter can be used instead of ``supercell_factor``. A list consisting of 3 integer values specifying the supercell size can be passed to the ``size`` arugment in that case (e.g., size = [3, 3, 3] for a $3 \\times 3 \\times 3$ supercell). \n",
    "\n",
    "The maximum size of clusters (i.e., number of atoms in a given cluster) and their maximum diameters are specified using ``max_cluster_size`` and ``max_cluster_dia``, respectively. As empty and 1-body clusters do not need\n",
    "diameters in specifying the clusters, maximum diameters of clusters starting from 2-body clusters are specified in ``max_cluster_dia`` in ascending order.\n",
    "\n",
    "The script above generates 16 entries to the database file with their names assigned as \"templates\". These templates are used to generate new structures and also to calculate their correlation functions. You can see the contents of the database file using the command below. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "autoscroll": false,
    "ein.hycell": false,
    "ein.tags": "worksheet-0",
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "!ase db aucu.db -c +name,size -L 0"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "ein.tags": "worksheet-0",
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "You can also view the clusters using the following command. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "autoscroll": false,
    "ein.hycell": false,
    "ein.tags": "worksheet-0",
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "# you can ignore the warning message\n",
    "setting.view_clusters()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There are several flavors of cluter expansion formalism in specifying the basis function for setting the site variable. Three types of basis functions are currently supported in ASE. The type of basis function can be selected by passing one of \"sanchez\", \"vandewalle\" and \"sluiter\" to ``basis_function`` argument (default is \"sanchez\" if you do not specify). More information on each basis function can be found in the following articles.\n",
    "\n",
    "\"sanchez\":\n",
    "> Sanchez, J. M., Ducastelle, F. and Gratias, D. (1984)<br>\n",
    "> [\"Generalized cluster description of multicomponent systems\"](https://doi.org/10.1016/0378-4371(84)90096-7)<br>\n",
    "> Physica A: Statistical Mechanics and Its Applications, 128(1-2), 334-350.\n",
    "\n",
    "\"vandewalle\":\n",
    "\n",
    "> van de Walle, A. (2009)<br>\n",
    "> [\"Multicomponent multisublattice alloys, nonconfigurational entropy and other additions to the Alloy Theoretic Automated Toolkit\"](https://doi.org/10.1016/j.calphad.2008.12.005)<br>\n",
    "> Calphad, 33(2), 266-278.\n",
    "\n",
    "\"sluiter\":\n",
    "> Zhang, X. and Sluiter M. (2016)<br>\n",
    "> [\"Cluster expansions for thermodynamics and kinetics of multicomponent alloys\"](https://doi.org/10.1007/s11669-015-0427-x)<br>\n",
    "> Journal of Phase Equilibria and Diffusion 37(1), 44-52.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "ein.tags": "worksheet-0",
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "## Generating initial pool of training structures \n",
    "\n",
    "The next step is to generate initial structures to start training the CE model. New structures for training\n",
    "CE model are generated using `NewStructures` class, which contains several methods for generating structures. The initial pool of structures is generated using `generate_initial_pool()` method."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "autoscroll": false,
    "ein.hycell": false,
    "ein.tags": "worksheet-0",
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "from ase.clease import NewStructures\n",
    "\n",
    "ns = NewStructures(setting=setting, generation_number=0,\n",
    "                   struct_per_gen=20)\n",
    "ns.generate_initial_pool()\n",
    "print(\"Done\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "ein.tags": "worksheet-0",
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "The ``NewStructures`` class takes setting instance and two additional arguments: \n",
    "   * ``generation_number`` is used to track at which point you generated the structures.\n",
    "   * ``struct_per_gen`` specifies the maximum number of structures to be generated for that generation number.\n",
    "   \n",
    "The `generate_initial_pool()` method first generates one structure per concentration where the number of each constituing element is at its maximum/minimum. In the case of Au-Cu alloy, there are two extrema: Au and Cu. Consequently, `generate_initial_pool()` first generates 2 structures for training. It subsequently generates 18 random structures (remaining number of structures in generation 0) by calling `generate_random_structures()` method. \n",
    "\n",
    "Optionally, you can specify the size of the supercell of the random structures by passing an ``Atoms`` object to ``atoms`` argument. For example, you can use script like below to enforce the supercell size of the random structures to be $3 \\times 3 \\times 3$.\n",
    "\n",
    "```python\n",
    "from ase.clease import NewStructures\n",
    "from ase.db import connect\n",
    "\n",
    "db = connect(\"aucu.db\")\n",
    "# get template with the cell size = 3x3x3\n",
    "template = db.get(\"name=template,size=3x3x3\").toatoms()\n",
    "\n",
    "ns = NewStructures(setting=setting, generation_number=0,\n",
    "                   struct_per_gen=20)\n",
    "ns.generate_initial_pool(atoms=template)\n",
    "```\n",
    "\n",
    "You can see that 20 generated structures are automatically stored in the database."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "autoscroll": false,
    "ein.hycell": false,
    "ein.tags": "worksheet-0",
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "!ase db aucu.db -c +name,size -L 0"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "ein.tags": "worksheet-0",
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "The generated structures include key-value pairs specifying their features. The genereated keys are:\n",
    "\n",
    "| key            | description                                                                       |\n",
    "|----------------|-----------------------------------------------------------------------------------|\n",
    "|``gen``         |generation number                                                                  |\n",
    "|``struct_type`` |\"initial\" for input structures, \"final\" for converged structures after calculation |\n",
    "|``size``        |size of the supercell                                                              |\n",
    "|``formula_unit``|reduced formula unit representation independent of the cell size                   |\n",
    "|``name``        |name of the structure (``formula_unit`` followed by a number)                      |\n",
    "|``converged``   |Boolean value indicating whether the calculation of the structure is converged     |\n",
    "|``queued``      |Boolean value indicating whether the calculation is queued in the workload manager |\n",
    "|``started``     |Boolean value indicating whether the calculation has started                       |\n",
    "\n",
    "The ``queued`` and ``started`` keys are useful for tracking the status of the calculations submitted using a workload manager, but they do not serve any purpose in this tutorial. \n",
    "\n",
    "You can see the keys of the new entries in the database file by modifying the ``ase db`` command above. \n",
    "\n",
    "For example, one can view generation number and structure type by appending ``gen`` and ``struct_type`` to the command.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "autoscroll": false,
    "ein.hycell": false,
    "ein.tags": "worksheet-0",
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "!ase db aucu.db -c +name,size,gen,struct_type -L 0"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "ein.tags": "worksheet-0",
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "## Running calculations on generated structures\n",
    "\n",
    "We use `EMT` calculator to demonstrate how one can run calculations on the structures generated using CLEASE and update database with the calculation results for further evaluation of the CE model. Here is a simple example script that runs the calculations for all structures that are not yet converged."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "autoscroll": false,
    "ein.hycell": false,
    "ein.tags": "worksheet-0",
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "from ase.calculators.emt import EMT\n",
    "from ase.db import connect\n",
    "from ase.clease.tools import update_db\n",
    "from ase.optimize import BFGS\n",
    "\n",
    "calc = EMT()\n",
    "db_name = \"aucu.db\"\n",
    "db = connect(db_name)\n",
    "\n",
    "# Run calculations for all structures that are not converged.\n",
    "for row in db.select(converged=False):\n",
    "    atoms = row.toatoms()\n",
    "    atoms.set_calculator(calc)\n",
    "    opt = BFGS(atoms, logfile=None)\n",
    "    opt.run(fmax=0.02)\n",
    "    update_db(uid_initial=row.id, final_struct=atoms, db_name=db_name)\n",
    "print(\"Done.\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "ein.tags": "worksheet-0",
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "CLEASE has `update_db` function in ``clease.tools`` to update the database entry with the calculation results. It automatically updates the key-value pairs of the initial structure entry as following:\n",
    "\n",
    "| key               | description                                               |\n",
    "|-------------------|-----------------------------------------------------------|\n",
    "|``converged``      |True                                                       |\n",
    "|``started``        |*empty*                                                    |\n",
    "|``queued``         |*empty*                                                    |\n",
    "|``final_struct_id``|ID of the DB entry containing the final converged structure|\n",
    "\n",
    "The script also generates new entries containing calculation results (i.e., energy of the structures) with same ``name`` value as its original entry. You can check the entries in the database file to see the new entries containing the calculation results."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "autoscroll": false,
    "ein.hycell": false,
    "ein.tags": "worksheet-0",
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "!ase db aucu.db -c +name,struct_type,final_struct_id -L 0"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "ein.tags": "worksheet-0",
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "## Evaluation of the CE model\n",
    "\n",
    "We are now ready to evaluate a CE model constructed from the initial 10 calculations. The evaluation of the CE model is performed using ``Evaluate`` class, and it supports 3 different linear regression schemes: Bayesian Compressive Sensing (BCS), $\\ell_1$ and $\\ell_2$ regularization. \n",
    "\n",
    "We will be trying out $\\ell_1$ and $\\ell_2$ regularization schemes to see how they perform using the script below. The script is written to use $\\ell_1$ regularization as a fitting scheme (i.e., fitting_scheme='l1'), and you can change the fitting scheme to $\\ell_2$ simply by changing it to 'l2'.  \n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "autoscroll": false,
    "ein.hycell": false,
    "ein.tags": "worksheet-0",
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "from ase.clease import Evaluate\n",
    "\n",
    "eva = Evaluate(setting=setting, fitting_scheme='l1', scoring_scheme='k-fold', nsplits=10)\n",
    "# scan different values of alpha and return the value of alpha that yields the lowest CV score\n",
    "alpha = eva.plot_CV(alpha_min=1E-7, alpha_max=1.0, num_alpha=50)\n",
    "\n",
    "# set the alpha value with the one found above, and fit data using it.\n",
    "eva.set_fitting_scheme(fitting_scheme='l1', alpha=alpha)\n",
    "eva.plot_fit(interactive=False)\n",
    "\n",
    "# plot ECI values, an emptry list is passed to ignore_sizes to plot all cluster sizes \n",
    "eva.plot_ECI(ignore_sizes=[], interactive=False)\n",
    "\n",
    "# print dictionary containing cluster names and their ECIs\n",
    "print(eva.get_cluster_name_eci())\n",
    "\n",
    "# save a dictionary containing cluster names and their ECIs\n",
    "eva.save_cluster_name_eci(fname='cluster_name_eci_l1.json')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "ein.tags": "worksheet-0",
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "## Generation of probe structures for further training structures\n",
    "\n",
    "You have now seen the initial cross validation (CV) score using 10 initial training structures. We can further train the CE model using more training structures to make it more robust. \n",
    "\n",
    "CLEASE supports 3 ways to generate more strucures. The first (and most obvious) is generating random structures as you have already done. The second method is to generate so called \"probe structures\" which differ the most from the existing training structures. The third method is to generate ground-state structures predicted based on current CE model.\n",
    "\n",
    "You can generate probe structures using the following script. Note that it internally uses simulated annealing algorithm which uses fictitious temperature values to maximize the difference in correlation function of the new structure. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "autoscroll": false,
    "ein.hycell": false,
    "ein.tags": "worksheet-0",
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "ns = NewStructures(setting=setting, generation_number=1,\n",
    "                   struct_per_gen=10)\n",
    "ns.generate_probe_structure()\n",
    "print(\"Done\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "ein.tags": "worksheet-0",
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "Once 10 additional structures are generated, you can re-run the script in \"Running calculations on generated structures\" section to calculate their energies. You should also run the script in \"Evaluation of the CE model\" section to evaluate the CV score of the model.\n",
    "\n",
    "It is likely that the CV score of the model is sufficiently low (few meV/atom or less) at this point. \n",
    "\n",
    "\n",
    "## Generate ground-state structures\n",
    "\n",
    "You can now genereate ground-state structures to construct convex-hull plot of formation energy. The script below generates ground-state structures with a cell size of 4x4x4 at random compositions based on current CE model."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "autoscroll": false,
    "ein.hycell": false,
    "ein.tags": "worksheet-0",
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "import json\n",
    "\n",
    "# get template with the cell size = 4x4x4\n",
    "template = db.get(\"name=template,size=4x4x4\").toatoms()\n",
    "\n",
    "# import dictionary containing cluster names and their ECIs\n",
    "with open('cluster_name_eci_l1.json') as f:\n",
    "    eci = json.load(f)\n",
    "\n",
    "ns = NewStructures(setting=setting, generation_number=2,\n",
    "                   struct_per_gen=10)\n",
    "\n",
    "ns.generate_gs_structure(atoms=template, init_temp=2000,\n",
    "                         final_temp=1, num_temp=10,\n",
    "                         num_steps_per_temp=5000,\n",
    "                         cluster_name_eci=eci,\n",
    "                         random_composition=True)\n",
    "print(\"Done\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "ein.tags": "worksheet-0",
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "Again, you should re-run the scripts in \"Running calculations on generated structures\" and \"Evaluation of the CE model\" sections to see the convex-hull plot and the latest CV score of the model. If you observe that the CV score is high (more than ~5 meV/atom), you may want to repeat running the script for generating ground-state structures. \n",
    "\n",
    "If you want to generate ground-state structures at the three concentrations in which stable ordered phase exist (Au$_3$Cu, AuCu, AuCu$_3$), you can alternatively run the script below.  "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "autoscroll": false,
    "ein.hycell": false,
    "ein.tags": "worksheet-0",
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "import json\n",
    "# get template with the cell size = 4x4x4\n",
    "# template consists only of Au atoms\n",
    "template = db.get(\"name=template,size=4x4x4\").toatoms()\n",
    "\n",
    "# count the number of atoms in the cell\n",
    "natoms = len(template)\n",
    "\n",
    "# -----------------------------------------------------#\n",
    "# generate Atoms object with the desired concentration #\n",
    "# -----------------------------------------------------#\n",
    "template_list = []\n",
    "\n",
    "# Au3Cu\n",
    "atoms = template.copy()\n",
    "for index in range(int(natoms/4)):\n",
    "    atoms[index].symbol = 'Cu'\n",
    "template_list.append(atoms)\n",
    "\n",
    "# AuCu\n",
    "atoms = template.copy()\n",
    "for index in range(int(natoms/2)):\n",
    "    atoms[index].symbol = 'Cu'\n",
    "template_list.append(atoms)\n",
    "\n",
    "# AuCu3\n",
    "atoms = template.copy()\n",
    "for index in range(int(3*natoms/4)):\n",
    "    atoms[index].symbol = 'Cu'\n",
    "template_list.append(atoms)\n",
    "\n",
    "\n",
    "# import dictionary containing cluster names and their ECIs\n",
    "with open('cluster_name_eci_l1.json') as f:\n",
    "    eci = json.load(f)\n",
    "\n",
    "ns = NewStructures(setting=setting, generation_number=3,\n",
    "                   struct_per_gen=3)\n",
    "\n",
    "ns.generate_gs_structure(atoms=template_list, init_temp=2000,\n",
    "                         final_temp=1, num_temp=10,\n",
    "                         num_steps_per_temp=5000,\n",
    "                         cluster_name_eci=eci,\n",
    "                         random_composition=False)\n",
    "print(\"Done\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "ein.tags": "worksheet-0",
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "Feel free to change the scripts above to experiment how you can search ground-state structures and construct a convex-hull plot!\n",
    "\n",
    "Note: if you have the converged CE model and found the ground-state structures in database already, the last script above will not be able to generate new ground-state structure as it already exists."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "autoscroll": false,
    "ein.hycell": false,
    "ein.tags": "worksheet-0",
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "## Checking the stable ordered phases\n",
    "\n",
    "The 3 stable ordered phases (Au$_3$Cu, AuCu and AuCu$_3$) are provided in this example, which you can visualize using the following script. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from ase.visualize import view\n",
    "from ase.io import read\n",
    "\n",
    "struct = []\n",
    "struct.append(read('Au3Cu1.xyz'))\n",
    "struct.append(read('Au1Cu1.xyz'))\n",
    "struct.append(read('Au1Cu3.xyz'))\n",
    "\n",
    "view(struct)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "After inspecting the provided structures, run the script below to visualize the minimum energy structures of Au$_3$Cu, AuCu and AuCu$_3$ that exists in your DB. Do they look the same or similar?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# find the minimum energy structures\n",
    "Au3Cu1 = []\n",
    "names = []\n",
    "for row in db.select('formula_unit=Au3Cu1,struct_type=initial'):\n",
    "    energy = db.get(id=row.final_struct_id).energy\n",
    "    energy /= row.natoms\n",
    "    Au3Cu1.append((row.name, energy))\n",
    "Au3Cu1.sort(key=lambda x: x[1])\n",
    "names.append(Au3Cu1[0][0])\n",
    "\n",
    "Au1Cu1 = []\n",
    "for row in db.select('formula_unit=Au1Cu1,struct_type=initial'):\n",
    "    energy = db.get(id=row.final_struct_id).energy\n",
    "    energy /= row.natoms\n",
    "    Au1Cu1.append((row.name, energy))\n",
    "Au1Cu1.sort(key=lambda x: x[1])\n",
    "names.append(Au1Cu1[0][0])\n",
    "\n",
    "Au1Cu3 = []\n",
    "for row in db.select('formula_unit=Au1Cu3,struct_type=initial'):\n",
    "    energy = db.get(id=row.final_struct_id).energy\n",
    "    energy /= row.natoms\n",
    "    Au1Cu3.append((row.name, energy))\n",
    "Au1Cu3.sort(key=lambda x: x[1])\n",
    "names.append(Au1Cu3[0][0])\n",
    "\n",
    "print(names)\n",
    "\n",
    "# view minimum energy structures\n",
    "images = []\n",
    "for name in names:\n",
    "    atoms = db.get('name={},struct_type=initial'.format(name)).toatoms()\n",
    "    images.append(atoms)\n",
    "view(images)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.3"
  },
  "name": "cluster_expansion_tutorial.master.ipynb"
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
